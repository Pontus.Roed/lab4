package cellular;
import java.util.Objects;
import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton{


	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return this.currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {

		return this.currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
	
		return this.currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < this.numberOfRows(); row++) {
			for (int col = 0; col < this.numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));

			}

		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);
        CellState currentCellState = this.getCellState(row, col);
		CellState nextCellState = CellState.DEAD;

        if (currentCellState == CellState.ALIVE) {
            nextCellState = CellState.DYING;
        }
        if (currentCellState == CellState.DEAD && livingNeighbors == 2) { 
            nextCellState = CellState.ALIVE;
        }



        return nextCellState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		for (int r = row -1 ; r <= row +1; r++) {
			for (int c = col - 1; c <= col +1; c++) {
				if (r < 0 || r >= this.numberOfRows()) {
					continue;
				}
				if (c < 0 || c >= this.numberOfColumns()) {
					continue;
				}
				if (r == row && c == col) {
					continue;
				}
				if ( Objects.equals(state, this.getCellState(r, c))) {
					count ++;
				}
			}
			
		}
		return count;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}