package datastructure;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {   
        this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];
        for (int row = 0; row < rows -1; row++) {
            for (int col = 0; col < cols; col++) {
                this.grid[row][col] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        
        return this.rows;
    }

    @Override
    public int numColumns() {
        
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        
        return this.grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid minVariabel = new CellGrid(this.rows, this.cols, null);
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                minVariabel.grid[row][col] = this.grid[row][col];
            }
            
        }
        return minVariabel;
    }
     
}
